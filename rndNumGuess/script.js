/*
Number guessing game from Treehouse JS Basics course.
Generates a random number between 1 and 6 and gives the player
two attempts to guess correctly.
*/

//Assume player guess = false.
var correctGuess = false;

// Generate random number.
var rndNum = Math.floor(Math.random() * 6) + 1;

// Prompt user for guess.
var guess = prompt('Enter a number between 1 and 6.');

/* Test for correct answer. If incorrect then test if higher or
lower and ask for another guess.
*/
if (parseInt(guess) === rndNum) {
  correctGuess = true;
} else if (parseInt(guess) < rndNum) {
  var guessMore = prompt('Try again, higher than ' + guess);
  if (parseInt(guessMore) === rndNum) {
    correctGuess = true;
  }
} else if (parseInt(guess) > rndNum) {
  var guessLess = prompt('Try again, lower than ' + guess);
  if (parseInt(guessLess) === rndNum) {
    correctGuess = true;
  }
}
/* If first or second guess was correct then print win message. If not,
then print losing message.
*/
if (correctGuess) {
  document.write('<p>Well done! You guessed the number.</p>');
} else {
  document.write('<p>Sorry, the number was ' + rndNum + ".</p>")
}
