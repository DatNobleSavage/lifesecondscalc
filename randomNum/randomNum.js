function getRandomNumber(lower, upper) {
  if (isNan(lower) || isNaN(upper)){
    throw new Error('One of the inputs is not a number.')
  }
  var randomNumber = Math.floor(Math.random() * (upper - lower + 1)) + lower;
  return randomNumber;
}



console.log(getRandomNumber(50, 75));
// console.log(getRandomNumber(6));
// console.log(getRandomNumber(100));
// console.log(getRandomNumber(10000));

// alert(getRandomNumber());
// console.log(getRandomNumber());
// var dieRoll = getRandomNumber();

// function getArea(width, length) {
//   var area = width * length;
//   return area;
// }

// console.log(getArea(10, 25));
